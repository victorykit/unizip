# victorykit/unizip

A uniform utility for creating ZIP archives via NodeJS through [Info-Zip](http://infozip.sourceforge.net/), or [AdmZip](https://github.com/cthackers/adm-zip).

Both will result in the roughly same output and share the same interface, however the compression rate may differ...

#### Javascript

```javascript
#!/usr/bin/env node

const unizip = require( '@victorykit/unizip' );

//uses InfoZip - archives node_modules directory with a `nodejs` path prefix
var archive = new unizip.InfoZipArchive( 'test.zip' );
archive.add( 'node_modules/**/*', 'nodejs' ).close();

//uses AdmZip - archives node_modules directory with a `nodejs` path prefix
var archive2 = new zip.AdmZipArchive( 'test2.zip' );
archive2.add( 'node_modules/**/*', 'nodejs' )
archive2.close();
```

#### Shell Script (POSIX)

```shell
#!/bin/sh

#uses InfoZip
node lib/zip.js test.zip 'node_modules/**/*' -p 'nodejs' -a 'infozip'

#uses AdmZip
node lib/zip.js test.zip 'node_modules/**/*' -p 'nodejs' -a 'admzip'

#for help
node lib/zip.js --help
```