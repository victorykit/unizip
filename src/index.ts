import * as child_process from 'child_process';
import * as fs from 'fs';
import * as os from 'os';
import * as path from 'path';
import AdmZip from 'adm-zip';
import * as glob from 'glob';
import yargs from 'yargs/yargs';


export interface IZipArchive {
  readonly outpath: string;
  readonly patterns: Promise<GlobPatternSpec>[];
}


export interface GlobPatternSpec {
  files: string[];
  basepath: string ;
  wd: string ;
}


export enum ArchiveMode {
  'w' = 0,
  'w+' = 1,
}


/**
 * copy a file (promisified)
 *
 * @param  {String} src         full origin path
 * @param  {String} dstdir      path to destination directory
 */
function copyFile( src:string, dstdir:string ) {

  return fs.promises.mkdir( dstdir, { recursive: true } )
    .then( () => {

      return fs.promises.copyFile(
        src,
        path.join( dstdir, path.basename( src ) ),
      );
    } );
}


/**
 * Zip archive base class
 *
 */
export abstract class ZipArchive implements IZipArchive {

  readonly outpath: string;
  readonly patterns: Promise<GlobPatternSpec>[];


  /**
     * mark files for archiving through a glob pattern
     *
     * @param  {String} wd          working directory for path discovery
     */
  constructor( outpath: string, mode?:ArchiveMode ) {

    mode = mode;

    this.outpath = path.resolve( process.cwd(), outpath );
    this.patterns = [];
  }


  /**
     * mark files for archiving through a glob pattern
     *
     * @param  {String} pattern     glob pattern to match paths against
     * @param  {String} basepath    destined path prefix for archive paths
     * @param  {String} wd          working directory for path discovery
     */
  add( pattern:string, basepath?:string, wd?:string ) {

    var promise:Promise<GlobPatternSpec> = new Promise( ( resolve, reject ) => {

      try {

        var files:string[] = glob.sync( pattern );

        basepath = ( basepath === undefined ) ? '' : basepath;
        wd = ( wd === undefined ) ? process.cwd() : wd;

        resolve( { files: files, basepath: basepath, wd: wd } );
      } catch ( err:any ) {

        reject( err );
      }
    } );

    this.patterns.push( promise );

    return this;
  }


  /**
     * mark files for archiving through a glob pattern
     *
     * @param  {String} pattern     glob pattern to match paths against
     * @param  {String} basepath    destined path prefix for archive paths
     * @param  {String} wd          working directory for path discovery
     */
  close(): Promise<void> {

    return Promise.all( this.patterns ).then( ( specs:GlobPatternSpec[] ) => {

      var promises:Promise<void>[] = [];

      specs.forEach( (spec:GlobPatternSpec) => {

        promises.push(
          this._write( spec.files, spec.basepath, spec.wd ),
        );
      } );

      return Promise.all( promises );
    } ).then( () => {

      return this._finalize();
    } );
  }


  abstract _write( files:string[], basepath:string, wd:string): Promise<void>

  abstract _finalize(): Promise<void>
}


/**
 * Adm-Zip Zip Archive
 *
 * create a Zip archive through using Adm-Zip npm package
 */
export class AdmZipArchive extends ZipArchive {

  readonly archiveObject: any;


  constructor( outpath: string, mode?:ArchiveMode ) {

    mode = mode;

    super( outpath );

    this.archiveObject = new AdmZip();
  }


  _write( files:string[], basepath:string, wd:string ): Promise<void> {

    wd = wd;

    return new Promise( ( resolve ) => {
      files.forEach( ( file:string ) => {

        if ( fs.lstatSync( file ).isFile() === false ) { return; }

        var outpath = path.join( basepath, file );
        if ( file[ 0 ] === path.sep ) {

          outpath = path.join( basepath, path.basename( file ) );
        }

        this.archiveObject.addLocalFile( file, path.dirname( outpath ) );

        resolve();
      } );
    } );
  }


  _finalize(): Promise<void> {

    return new Promise( ( resolve, reject) => {

      try {
        this.archiveObject.writeZip( this.outpath );

        resolve();
      } catch ( err:any ) {

        reject( err );
      }
    } );
  }
}


/**
 * InfoZip Archive
 *
 * create a Zip archive through using UN*X InfoZip executable
 */
export class InfoZipArchive extends ZipArchive {

  readonly tempDirPath: string;


  constructor( outpath: string, mode?:ArchiveMode ) {

    mode = mode;

    super( outpath );

    this.tempDirPath = fs.mkdtempSync( path.join( os.tmpdir(), 'zip-' ) );
  }


  _write( files:string[], basepath:string, wd:string ): Promise<void> {

    wd = wd;

    var promises:Promise<void>[] = [];

    files.forEach( ( file:string ) => {

      if ( fs.lstatSync( file ).isFile() === false ) { return; }

      var outpath = path.join( basepath, file );
      if ( file[ 0 ] === path.sep ) {

        outpath = path.join( basepath, path.basename( file ) );
      }

      promises.push(
        copyFile( file, path.dirname( path.join( this.tempDirPath, outpath ) ) ),
      );
    } );

    return Promise.all( promises ).then( () => {

    } );
  }


  _finalize(): Promise<void> {

    var oldcwd = process.cwd();

    process.chdir( this.tempDirPath );

    var cmd = `zip -r ${this.outpath} '.'`;

    return new Promise((resolve, reject) => {

      child_process.exec( cmd, ( error, stdout, stderr ) => {

        stdout = stdout;
        stderr = stderr;

        if (error) {

          console.warn( error );

          reject( error );
        }

        process.chdir( oldcwd );

        resolve();
      });
    });
  }
}


const parser = yargs(process.argv.slice(2) )
  .command('$0 zipfile <file..>', 'package and compress (archive) files')
  .options( {
    r: { type: 'boolean', alias: 'recurse-paths', default: false },
    p: { type: 'string', alias: 'prefix', default: undefined },
    a: { choices: ['infozip', 'admzip'], alias: 'archiver', default: 'admzip' },
  } );


export const scriptPath = __filename;


if ( require.main === module ) {

  await ( async( ) => {
    const argv = await parser.argv;

    var archive:ZipArchive;

    switch ( argv.archiver as string ) {

      case 'admzip':
        archive = new AdmZipArchive( argv.zipfile as string );
        break;

      case 'infozip':
        archive = new InfoZipArchive( argv.zipfile as string );
        break;
      default:
        throw new Error( 'wrong' );
    }

    var files = ( argv.file as string[] );

    for ( var i = 0; i < files.length; i = i + 1 ) {

      archive.add( files[ i], argv.prefix as string );
    }

    await archive.close();
  } )();

}


